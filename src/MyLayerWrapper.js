import XYZ from "ol/source/XYZ";
import TileLayer from "ol/layer/Tile";

export default class MyLayerWrapper extends TileLayer {

    // ...a class that is backed by an `olLayer` / `olSource`, but has some custom logic

    redraw() {
        const source = this.source;
        if (source) {
            if (source.getTileLoadFunction) {
                // This forces a tileCache.clear(), something we can't do directly in production builds.
                // See: https://github.com/openlayers/ol3/issues/5032
                source.setTileLoadFunction(source.getTileLoadFunction());
            } // end if

            // If the above doesn't work, this should.
            source.changed();
        } // end if
    } // end redraw
}