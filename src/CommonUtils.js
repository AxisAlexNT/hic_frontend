export default class CommonUtils {
    static clamp(x, min, max) {
        return Math.min(Math.max(x, min), max);
    }

    static clampSquareCoordinates(a, min, max) {
        return a.map(e => this.clamp(e, min, max));
    }
}